package com.edurabroj.s15_mapas;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {
    private Button btnUbicacion;

    private TextView
            tvPrecision,
            tvTiempo,
            tvLongitud,
            tvLatitud;

//    Location mejorUbicacion;
    LocationManager locationManager;
//    LocationListener locationListener;

    public static final float ULTIMA_PRECISION_LEIDA = 500.0f;
    public static final long UN_MINUTO = 1000*60;
    public static final long CINCO_MINUTOS = UN_MINUTO*5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnUbicacion = findViewById(R.id.btnUbicacion);
        tvPrecision = findViewById(R.id.tvPrecision);
        tvTiempo = findViewById(R.id.tvTiempo);
        tvLongitud = findViewById(R.id.tvLongitud);
        tvLatitud = findViewById(R.id.tvLatitud);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        btnUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarUbicacion(getMejorUbicacionConocida(ULTIMA_PRECISION_LEIDA,CINCO_MINUTOS));
            }
        });

//        mejorUbicacion = getMejorUbicacionConocida(ULTIMA_PRECISION_LEIDA,CINCO_MINUTOS);
//        mostrarUbicacion(mejorUbicacion);
    }

    private Location getMejorUbicacionConocida(float minPrecision, long maxTiempo){
//        Location mejorUbicacion;
//        float mejorPrecision = Float.MAX_VALUE;
//        float mejorTiempo = Float.MIN_VALUE;

        List<String> proveedores = locationManager.getAllProviders();

        for(String proveedor : proveedores){
            if(ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)!= PERMISSION_GRANTED &&
               ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION)!= PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,
                        new String[]{
                            ACCESS_FINE_LOCATION,
                            ACCESS_COARSE_LOCATION
                        },
                        1);
                return null;
            }

            return locationManager.getLastKnownLocation(proveedor);
//            Location ubicacion = locationManager.getLastKnownLocation(proveedor);

//            if(ubicacion!=null){
//                float precision = ubicacion.getAccuracy();
//                float tiempo = ubicacion.getTime();
//
//                mejorUbicacion = ubicacion;
//                mejorPrecision = precision;
//                mejorTiempo = tiempo;
//            }
        }
        return null;
    }

    private void mostrarUbicacion(Location ubicacion) {
        if(ubicacion==null){
            Toast.makeText(this,"Acepta pe",Toast.LENGTH_SHORT).show();
            return;
        }

        tvPrecision.setText("Precisión: " + ubicacion.getAccuracy());
        tvTiempo.setText("Tiempo: " + ubicacion.getTime());
        tvLatitud.setText("Latitud: " + ubicacion.getLatitude());
        tvLongitud.setText("Longitud: " + ubicacion.getLongitude());
    }
}
